'use strict';
const store = uniCloud.database()
exports.main = async (event, context) => {
	const {
		id,
		...prop
	} = event;
	return store.collection('todolists').doc(id).update(prop)
};
